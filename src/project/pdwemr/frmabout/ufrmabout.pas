unit ufrmabout;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  Tfrmabout = class(TForm)
    lbl1: TLabel;
    lbl2: TLabel;
    edtjiqm: TEdit;
    btn1: TButton;
    procedure btn1Click(Sender: TObject);
  private

  public
    procedure execute(strlocalcode : string);
  end;

var
  frmabout: Tfrmabout;

implementation

{$R *.dfm}

{ Tfrmabout }

procedure Tfrmabout.execute(strlocalcode: string);
begin
  edtjiqm.Text := strlocalcode;
  ShowModal;
end;

procedure Tfrmabout.btn1Click(Sender: TObject);
begin
  ModalResult := mrOk;
end;

end.

