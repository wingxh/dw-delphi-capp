inherited frmmain: Tfrmmain
  Left = 3
  Top = 104
  Width = 1382
  Height = 744
  Caption = #33268#36719#36719#20214#25253#34920#35774#35745#22120
  FormStyle = fsMDIForm
  OldCreateOrder = True
  WindowState = wsMaximized
  PixelsPerInch = 96
  TextHeight = 14
  object upspltr2: TUpSplitter [0]
    Left = 197
    Top = 44
    Width = 2
    Height = 662
  end
  object ptop: TUPanel [1]
    Left = 0
    Top = 0
    Width = 1366
    Height = 44
    Align = alTop
    ParentColor = True
    TabOrder = 0
    object lbl1: TUpLabel
      Left = 14
      Top = 11
      Width = 198
      Height = 21
      Caption = #33268#36719#36719#20214#25253#34920#35774#35745#22120
      Font.Charset = GB2312_CHARSET
      Font.Color = clWindowText
      Font.Height = -21
      Font.Name = #23435#20307
      Font.Style = [fsBold]
      ParentFont = False
    end
    object btndesign: TUpSpeedButton
      Left = 235
      Top = 8
      Width = 119
      Height = 28
      Caption = #35774#35745#25253#34920'(&S)'
      Flat = True
      Glyph.Data = {
        2A030000424D2A030000000000002A0200002800000010000000100000000100
        08000000000000010000120B0000120B00007D0000007D00000000000000FFFF
        FF00FF00FF009C573B008E472200A0502700944A24008F4823007F3F1F00954B
        2500944A250083422100804020007F402000944D2900A3562E0091492300A151
        27007F401F00964C250085452300A95A2E00AA5A2F00AA5B2F00AE5F3200AA58
        2700AF603100AE5C2700B95D1900B56D3800B26C3900FFFBF800B37D5300FFF7
        F000FFF8F200DB7F2600DA7F2600D87E2600DC822C00F6BB8000FFF1E300FFF3
        E700FEF2E600FFF5EB00FFF6ED00FFF7EF00FFF8F100FFFAF500FEF9F4008886
        8400FFFDFB00E78A2100FDD7AC00FEDCB600FEDEBA00FDDDBA00FDDFBE00FEE3
        C600FEE4C700FFE6CA00FEE5CA00FEE6CB00FEE7CD00FEE9D200FEEAD500FEEB
        D600FEECD800FFEDDA00FEEEDC00FFEFDE00FEEEDD00FFF0E000FEF6ED00FCD9
        B000FDDAB100FEDCB500FDDDB700FDDFBB00FDDFBD00FDE0BE00FEE1C000FDE0
        BF00FDE1C000FEE2C200FEE3C400FDE3C500FEE5C800FEE6CA00FEE8CF00FEE9
        D100FDE8D000FEEAD300FEECD600FDEBD500FFEEDA00FFF0DF00FEF0E000FEF1
        E100FEF2E400FEF4E800FEF5EA00FFF8F000FFF9F200FCEDDA00FEF2E200FEF2
        E300FEF5E900F3EDE500FFFBF600D3A35C00D4A45D00D3A35D00FEF5E800D7A9
        6100D7A96200D9AE6C00D8AE6D00CFA76900CAAA7700FFFCF700FEFBF500FEFD
        F900006600004E6ED4000033FF00020202020202020202020202020202020509
        0A060606060606060606060613110B2C2931675D313B3B313649313434100D6B
        31313131313131313131313131070C31317A7028465C59575451374B4A070C48
        31652B2A4743407C56534E4C7C071231317C2D6A6945423F3D39523535071266
        31782E487A61447A583C7C387A070C313177302164625F423F3E7A504D070832
        31796C227C636844415A57554F04143131321F2F212B2A605E5B3E3A530E1E71
        6F6D6D6D6D6D6D6E74727375761D1B252324242424242426273327207B1C0215
        161616161616161718191A0F0302020202020202020202020202020202020202
        0202020202020202020202020202}
      OnClick = btndesignClick
      alignleftorder = 0
      alignrightorder = 0
    end
    object btnquit: TUpSpeedButton
      Left = 361
      Top = 8
      Width = 99
      Height = 28
      Caption = #20851#38381'(&Q)'
      Flat = True
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000120B0000120B00000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FF01079F0313A90418AE0419AE0313A90108A0FF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF01049D041CB10730C00734C407
        35C50735C50734C30731C1041FB301069EFF00FFFF00FFFF00FFFF00FFFF00FF
        0109A1052BC30735C70733C20732C20732C20732C20732C20733C30735C4062D
        BE020CA4FF00FFFF00FFFF00FF01049B052BCA0636D80431CD0027C4032EC107
        32C20732C20430C10027BF042FC10735C4072EBE01069EFF00FFFF00FF031ABA
        0537E70331DD123DD86480E01840CB002CC1022DC00F38C46580D91B43C7052F
        C10735C5051FB3FF00FF01049E0430E40436F1002AE45070E9FFFFFFB7C4F10D
        36CA042DC3A2B2E8FFFFFF6984DA0026BE0733C30731C10108A0020FAF0336FA
        0335F80232EE0A35E88CA2F2FFFFFFB4C2F1A9B8EDFFFFFFA7B7E9133AC4052F
        C10732C20734C40313AA0619BC1747FE093AFC0435F80131F0002BE891A5F4FF
        FFFFFFFFFFABBAEF062FC5022DC00732C20732C20736C50419AE0B1DBE4168FE
        1C49FC0335FB0031F90531F2A4B5F7FFFFFFFFFFFFB9C6F20D36D0002CC60732
        C20732C20736C50418AD0613B45B7CFC486CFD0133FB113CFBA1B4FEFFFFFFA4
        B6F892A7F5FFFFFFB6C4F21A41D3042FC80732C40734C30212A90003A04A6AF3
        8FA6FF1F46FB4C6FFCFFFFFFA7B8FE0733F6002AED8CA2F6FFFFFF627FE70028
        D00734CC0730C300069FFF00FF1A2FCB99AFFF8BA2FE214DFB4D71FC0E3DFB00
        30FB0031F70636F14C6EF1103CE30432DB0636D7041CB5FF00FFFF00FF0004A0
        415EECB8C7FF9CAFFD3A5CFC0A3AFB0335FB0335FB0133F9052FF20635EB0537
        E9052CCD00049CFF00FFFF00FFFF00FF0309A54260ECA9BBFFBDCAFF8EA5FE64
        83FD5073FC4A6EFD3961FD1444F9042CD70109A2FF00FFFF00FFFF00FFFF00FF
        FF00FF0004A01E32CD5876F6859EFE8BA3FF7994FE5376FC234AF0051EC50104
        9CFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF0004A00917B610
        22C30D1FC20311B401059FFF00FFFF00FFFF00FFFF00FFFF00FF}
      OnClick = btnquitClick
      alignleftorder = 0
      alignrightorder = 0
    end
  end
  object pleft: TUPanel [2]
    Left = 0
    Top = 44
    Width = 197
    Height = 662
    Align = alLeft
    ParentColor = True
    TabOrder = 1
    object p2: TUpAdvToolPanel
      Left = 1
      Top = 1
      Width = 195
      Height = 58
      Align = alTop
      BackgroundTransparent = False
      BackGroundPosition = bpTopLeft
      BorderWidth = 1
      Button3D = False
      HoverButtonColor = 16571329
      HoverButtonColorTo = 16565398
      DownButtonColor = 16571329
      DownButtonColorTo = 16565398
      CaptionButton = False
      Color = 16571329
      ColorTo = 16571329
      GradientDirection = gdVertical
      DockDots = True
      CanSize = False
      Caption = #35831#36873#25321#27169#22359
      CaptionGradientDirection = gdVertical
      FocusCaptionFontColor = clBlack
      FocusCaptionColor = 16571329
      FocusCaptionColorTo = 16565398
      NoFocusCaptionFontColor = clBlack
      NoFocusCaptionColor = 16571329
      NoFocusCaptionColorTo = 16565398
      CloseHint = 'Close panel'
      LockHint = 'Lock panel'
      UnlockHint = 'Unlock panel'
      Sections = <>
      SectionLayout.CaptionColor = 16244422
      SectionLayout.CaptionColorTo = 14060643
      SectionLayout.CaptionFontColor = 8661248
      SectionLayout.CaptionRounded = False
      SectionLayout.Corners = scRectangle
      SectionLayout.BackGroundColor = 16248798
      SectionLayout.BackGroundColorTo = 16242365
      SectionLayout.BorderColor = clWhite
      SectionLayout.BorderWidth = 1
      SectionLayout.BackGroundGradientDir = gdVertical
      SectionLayout.Indent = 4
      SectionLayout.Spacing = 4
      SectionLayout.ItemFontColor = 11876608
      SectionLayout.ItemHoverTextColor = 11876608
      SectionLayout.ItemHoverUnderline = True
      SectionLayout.UnderLineCaption = False
      ShowCaptionBorder = False
      ShowClose = False
      ShowLock = False
      Style = esCustom
      Version = '1.5.1.0'
      OrgHeight = 0
      object cbbmok: TUpComboBox
        Left = 8
        Top = 29
        Width = 182
        Height = 22
        Style = csDropDownList
        DropDownCount = 80
        ImeName = #20013#25991' ('#31616#20307') - '#25628#29399#25340#38899#36755#20837#27861
        ItemHeight = 14
        TabOrder = 0
        OnChange = cbbmokChange
        AutoInit = False
        HistoryValue = -1
        HistoryValueOnOff = True
      end
    end
    object p3: TUpAdvToolPanel
      Left = 1
      Top = 59
      Width = 195
      Height = 176
      Align = alTop
      BackgroundTransparent = False
      BackGroundPosition = bpTopLeft
      BorderWidth = 1
      Button3D = False
      HoverButtonColor = 16571329
      HoverButtonColorTo = 16565398
      DownButtonColor = 16571329
      DownButtonColorTo = 16565398
      CaptionButton = False
      Color = 16571329
      ColorTo = 16571329
      GradientDirection = gdVertical
      DockDots = True
      CanSize = False
      Caption = #35831#36873#25321#31383#20307
      CaptionGradientDirection = gdVertical
      FocusCaptionFontColor = clBlack
      FocusCaptionColor = 16571329
      FocusCaptionColorTo = 16565398
      NoFocusCaptionFontColor = clBlack
      NoFocusCaptionColor = 16571329
      NoFocusCaptionColorTo = 16565398
      CloseHint = 'Close panel'
      LockHint = 'Lock panel'
      UnlockHint = 'Unlock panel'
      Sections = <>
      SectionLayout.CaptionColor = 16244422
      SectionLayout.CaptionColorTo = 14060643
      SectionLayout.CaptionFontColor = 8661248
      SectionLayout.CaptionRounded = False
      SectionLayout.Corners = scRectangle
      SectionLayout.BackGroundColor = 16248798
      SectionLayout.BackGroundColorTo = 16242365
      SectionLayout.BorderColor = clWhite
      SectionLayout.BorderWidth = 1
      SectionLayout.BackGroundGradientDir = gdVertical
      SectionLayout.Indent = 4
      SectionLayout.Spacing = 4
      SectionLayout.ItemFontColor = 11876608
      SectionLayout.ItemHoverTextColor = 11876608
      SectionLayout.ItemHoverUnderline = True
      SectionLayout.UnderLineCaption = False
      ShowCaptionBorder = False
      ShowClose = False
      ShowLock = False
      Style = esCustom
      Version = '1.5.1.0'
      OrgHeight = 0
      object lstform: TUpListBox
        Left = 2
        Top = 25
        Width = 191
        Height = 149
        Align = alClient
        Font.Charset = GB2312_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = #23435#20307
        Font.Style = []
        ImeName = #20013#25991' ('#31616#20307') - '#25628#29399#25340#38899#36755#20837#27861
        ItemHeight = 15
        ParentFont = False
        TabOrder = 0
        OnClick = lstformDblClick
        OnDblClick = lstformDblClick
        AutoInit = False
        HistoryValue = -1
        HistoryValueOnOff = True
      end
    end
    object p4: TUpAdvToolPanel
      Left = 1
      Top = 235
      Width = 195
      Height = 426
      Align = alClient
      BackgroundTransparent = False
      BackGroundPosition = bpTopLeft
      BorderWidth = 1
      Button3D = False
      HoverButtonColor = 16571329
      HoverButtonColorTo = 16565398
      DownButtonColor = 16571329
      DownButtonColorTo = 16565398
      CaptionButton = False
      Color = 16571329
      ColorTo = 16571329
      GradientDirection = gdVertical
      DockDots = True
      CanSize = False
      Caption = #35831#36873#25321#25253#34920
      CaptionGradientDirection = gdVertical
      FocusCaptionFontColor = clBlack
      FocusCaptionColor = 16571329
      FocusCaptionColorTo = 16565398
      NoFocusCaptionFontColor = clBlack
      NoFocusCaptionColor = 16571329
      NoFocusCaptionColorTo = 16565398
      CloseHint = 'Close panel'
      LockHint = 'Lock panel'
      UnlockHint = 'Unlock panel'
      Sections = <>
      SectionLayout.CaptionColor = 16244422
      SectionLayout.CaptionColorTo = 14060643
      SectionLayout.CaptionFontColor = 8661248
      SectionLayout.CaptionRounded = False
      SectionLayout.Corners = scRectangle
      SectionLayout.BackGroundColor = 16248798
      SectionLayout.BackGroundColorTo = 16242365
      SectionLayout.BorderColor = clWhite
      SectionLayout.BorderWidth = 1
      SectionLayout.BackGroundGradientDir = gdVertical
      SectionLayout.Indent = 4
      SectionLayout.Spacing = 4
      SectionLayout.ItemFontColor = 11876608
      SectionLayout.ItemHoverTextColor = 11876608
      SectionLayout.ItemHoverUnderline = True
      SectionLayout.UnderLineCaption = False
      ShowCaptionBorder = False
      ShowClose = False
      ShowLock = False
      Style = esCustom
      Version = '1.5.1.0'
      OrgHeight = 0
      object lstreport: TUpListBox
        Left = 2
        Top = 25
        Width = 191
        Height = 399
        Align = alClient
        Font.Charset = GB2312_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = #23435#20307
        Font.Style = []
        ImeName = #20013#25991' ('#31616#20307') - '#25628#29399#25340#38899#36755#20837#27861
        ItemHeight = 15
        ParentFont = False
        TabOrder = 0
        OnClick = lstreportDblClick
        OnDblClick = lstreportDblClick
        AutoInit = False
        HistoryValue = -1
        HistoryValueOnOff = True
      end
    end
  end
  object pclient: TUPanel [3]
    Left = 199
    Top = 44
    Width = 1167
    Height = 662
    Align = alClient
    BevelOuter = bvNone
    ParentColor = True
    TabOrder = 2
    object upspltr1: TUpSplitter
      Left = 0
      Top = 323
      Width = 1167
      Height = 2
      Cursor = crVSplit
      Align = alTop
    end
    object p5: TUpAdvToolPanel
      Left = 0
      Top = 0
      Width = 1167
      Height = 323
      Align = alTop
      BackgroundTransparent = False
      BackGroundPosition = bpTopLeft
      BorderWidth = 1
      Button3D = False
      HoverButtonColor = 16571329
      HoverButtonColorTo = 16565398
      DownButtonColor = 16571329
      DownButtonColorTo = 16565398
      CaptionButton = False
      Color = 16571329
      ColorTo = 16571329
      GradientDirection = gdVertical
      DockDots = True
      CanSize = False
      Caption = #25968#25454#38598#38598#21512
      CaptionGradientDirection = gdVertical
      FocusCaptionFontColor = clBlack
      FocusCaptionColor = 16571329
      FocusCaptionColorTo = 16565398
      NoFocusCaptionFontColor = clBlack
      NoFocusCaptionColor = 16571329
      NoFocusCaptionColorTo = 16565398
      CloseHint = 'Close panel'
      LockHint = 'Lock panel'
      UnlockHint = 'Unlock panel'
      Sections = <>
      SectionLayout.CaptionColor = 16244422
      SectionLayout.CaptionColorTo = 14060643
      SectionLayout.CaptionFontColor = 8661248
      SectionLayout.CaptionRounded = False
      SectionLayout.Corners = scRectangle
      SectionLayout.BackGroundColor = 16248798
      SectionLayout.BackGroundColorTo = 16242365
      SectionLayout.BorderColor = clWhite
      SectionLayout.BorderWidth = 1
      SectionLayout.BackGroundGradientDir = gdVertical
      SectionLayout.Indent = 4
      SectionLayout.Spacing = 4
      SectionLayout.ItemFontColor = 11876608
      SectionLayout.ItemHoverTextColor = 11876608
      SectionLayout.ItemHoverUnderline = True
      SectionLayout.UnderLineCaption = False
      ShowCaptionBorder = False
      ShowClose = False
      ShowLock = False
      Style = esCustom
      Version = '1.5.1.0'
      OrgHeight = 0
      object p12: TUPanel
        Left = 2
        Top = 25
        Width = 1163
        Height = 41
        Align = alTop
        BevelOuter = bvLowered
        ParentColor = True
        TabOrder = 0
        object btn1: TUpSpeedButton
          Left = 9
          Top = 6
          Width = 100
          Height = 28
          Caption = #20445#23384
          Flat = True
          Glyph.Data = {
            36030000424D3603000000000000360000002800000010000000100000000100
            18000000000000030000120B0000120B00000000000000000000FF00FFFF00FF
            FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
            FFFF00FFFF00FFFF00FFFF00FFFF00FF004B00004B00A18283A18283A18283A1
            8283A18283A18283A18283004B00004B00FF00FFFF00FFFF00FFFF00FF004B00
            008100008100DDD4D5004B00004B00DCE0E0D7DADECED5D7BDBABD004B000163
            01004B00FF00FFFF00FFFF00FF004B00008100008100E2D9D9004B00004B00D9
            D8DAD9DEE1D3D9DCC1BDC1004B00016301004B00FF00FFFF00FFFF00FF004B00
            008100008100E6DCDC004B00004B00D5D3D5D8DEE1D7DDE0C6C2C5004B000163
            01004B00FF00FFFF00FFFF00FF004B00008100008100EADEDEE7DDDDDDD4D5D7
            D3D5D5D7D9D7D8DACAC2C5004B00016301004B00FF00FFFF00FFFF00FF004B00
            0081000081000081000081000081000081000081000081000081000081000081
            00004B00FF00FFFF00FFFF00FF004B000081009DC29D9DC29D9DC29D9DC29D9D
            C29D9DC29D9DC29D9DC29D9DC29D008100004B00FF00FFFF00FFFF00FF004B00
            008100F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F70081
            00004B00FF00FFFF00FFFF00FF004B00008100F7F7F7F7F7F7F7F7F7F7F7F7F7
            F7F7F7F7F7F7F7F7F7F7F7F7F7F7008100004B00FF00FFFF00FFFF00FF004B00
            008100F7F7F7BFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFF7F7F70081
            00004B00FF00FFFF00FFFF00FF004B00008100F7F7F7F7F7F7F7F7F7F7F7F7F7
            F7F7F7F7F7F7F7F7F7F7F7F7F7F7008100004B00FF00FFFF00FFFF00FF004B00
            008100F7F7F7BFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFF7F7F70081
            00004B00FF00FFFF00FFFF00FF004B00008100F7F7F7F7F7F7F7F7F7F7F7F7F7
            F7F7F7F7F7F7F7F7F7F7F7F7F7F7008100004B00FF00FFFF00FFFF00FFFF00FF
            004B00F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7004B
            00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
            00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
          OnClick = btn1Click
          alignleftorder = 0
          alignrightorder = 0
        end
        object btn2: TUpSpeedButton
          Left = 113
          Top = 6
          Width = 100
          Height = 28
          Caption = #21462#28040
          Flat = True
          Glyph.Data = {
            36030000424D3603000000000000360000002800000010000000100000000100
            18000000000000030000120B0000120B00000000000000000000FF00FFFF00FF
            FF00FFFF00FFFF00FF652814672913672913672913672913672913FF00FFFF00
            FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF682913672913BC4B00BC4B00BC
            4B00BC4B00BC4B00BC4B00672913672913FF00FFFF00FFFF00FFFF00FFFF00FF
            83350BB54701BB4A00BB4A00BB4A00BB4A00BB4A00BC4B00BC4B00BC4B00B548
            01672913FF00FFFF00FFFF00FF81340CB44700B84800B64600B64600B64600B6
            4600B84800BA4900BC4B00BC4B00BC4B00B54801672913FF00FFFF00FFA94202
            B64700B44500B14300AD4100C06928CA7D40CC7F41CA7632C25C10BC4B00BC4B
            00BC4B00682913FF00FF923A07BA4C02B74D03B54D03B14B03B04901E0B289FE
            FEFEFEFEFEFEFEFEFEFEFED0813DBC4B00BC4B00BC4B00652814973D06BF5609
            BB570BB8570CB7560CB5540AB5540BB6560DB4530CB5520FDCA474FEFEFEBB4A
            00BC4B00BC4B00682913963E07C76A1FC1651ABF6419BF651BBD6318E3BD97B6
            550CB14D04AD4501B64E09FEFEFEBB4A00BC4B00BC4B00682913973F09D48B49
            CB7A34C67229C7742CE6C19DFEFEFEBC6017B6550CB24C04CB8042FEFEFEBB4A
            00BC4B00BC4B00682913953D07DEA068DA9D62CF803AF0D7BDFEFEFEFEFEFEF6
            E9DAEFD8C1EED5BDFEFEFEDDA574BB4A00BC4B00BC4B00682913953B05E09E63
            EBC6A1DD9C5EF2D8BDFEFEFEFEFEFEF0D9C2E5BC96E2B78ED08745B74801BB4A
            00BC4B00BC4B00652814FF00FF9A4711EFCEADF0CEACE3A972EECCABFEFEFEC9
            752DC2681DBF5C10BC5407BB4B01BB4A00BC4B00682913FF00FFFF00FF953B05
            E2A468F6E0C9F3D5B8E2A972EAC199CB762DC66A1EC25F11C1590ABC4D03BC4B
            00B54801632815FF00FFFF00FFFF00FF963C06E5A66CF3D7BBF4DEC7EBC49DE1
            A670DA9556D58945CE762CC05607B54801762F0FFF00FFFF00FFFF00FFFF00FF
            FF00FF973D069F4C16E6AA72EBBC90E9BB8EE3AA75D88B48C764169C41096F2D
            11FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF993F079C420A9E
            460E9D450C9C410984360CFF00FFFF00FFFF00FFFF00FFFF00FF}
          OnClick = btn2Click
          alignleftorder = 0
          alignrightorder = 0
        end
      end
      object dbgdataset: TUpWWDbGrid
        Left = 2
        Top = 66
        Width = 1163
        Height = 255
        ControlType.Strings = (
          'dayjlksd;CustomEdit;cbbdayjlksd;F'
          'dayjljsd;CustomEdit;cbbdayjljsd;F')
        Selected.Strings = (
          'bianm'#9'10'#9#32534#30721#9'T'
          'shujjyhmc'#9'18'#9#21517#31216#9'T'
          'shujjms'#9'32'#9#25551#36848#9'T'
          'paixh'#9'18'#9#25490#24207'ID'#9'T'
          'dayjlksd'#9'18'#9#25171#21360#35760#24405#24320#22987#28857#9#9
          'dayjljsd'#9'18'#9#25171#21360#35760#24405#32467#26463#28857#9#9
          'dayjlts'#9'18'#9#25171#21360#26465#25968#9#9)
        IniAttributes.Delimiter = ';;'
        TitleColor = 16571329
        FixedCols = 4
        ShowHorzScrollBar = True
        Align = alClient
        DataSource = dsdateset
        ImeName = #20013#25991' ('#31616#20307') - '#25628#29399#25340#38899#36755#20837#27861
        KeyOptions = []
        PopupMenu = pmdataset
        RowHeightPercent = 120
        TabOrder = 1
        TitleAlignment = taCenter
        TitleFont.Charset = GB2312_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -14
        TitleFont.Name = #23435#20307
        TitleFont.Style = []
        TitleLines = 1
        TitleButtons = True
        UseTFields = False
      end
      object cbbdayjlksd: TwwDBComboBox
        Left = 252
        Top = 112
        Width = 106
        Height = 22
        ShowButton = True
        Style = csDropDown
        MapList = True
        AllowClearKey = False
        DropDownCount = 8
        ImeName = #20013#25991' ('#31616#20307') - '#25628#29399#25340#38899#36755#20837#27861
        ItemHeight = 0
        Items.Strings = (
          'first'#9'1'
          'current'#9'2')
        Sorted = False
        TabOrder = 2
        UnboundDataType = wwDefault
      end
      object cbbdayjljsd: TwwDBComboBox
        Left = 372
        Top = 112
        Width = 105
        Height = 22
        ShowButton = True
        Style = csDropDown
        MapList = True
        AllowClearKey = False
        DropDownCount = 8
        ImeName = #20013#25991' ('#31616#20307') - '#25628#29399#25340#38899#36755#20837#27861
        ItemHeight = 0
        Items.Strings = (
          'current'#9'2'
          'last'#9'3'
          'recount'#9'4')
        Sorted = False
        TabOrder = 3
        UnboundDataType = wwDefault
      end
    end
    object p6: TUpAdvToolPanel
      Left = 0
      Top = 325
      Width = 1167
      Height = 337
      Align = alClient
      BackgroundTransparent = False
      BackGroundPosition = bpTopLeft
      BorderWidth = 1
      Button3D = False
      HoverButtonColor = 16571329
      HoverButtonColorTo = 16565398
      DownButtonColor = 16571329
      DownButtonColorTo = 16565398
      CaptionButton = False
      Color = 16571329
      ColorTo = 16571329
      GradientDirection = gdVertical
      DockDots = True
      CanSize = False
      Caption = #25253#34920#21464#37327#38598#21512
      CaptionGradientDirection = gdVertical
      FocusCaptionFontColor = clBlack
      FocusCaptionColor = 16571329
      FocusCaptionColorTo = 16565398
      NoFocusCaptionFontColor = clBlack
      NoFocusCaptionColor = 16571329
      NoFocusCaptionColorTo = 16565398
      CloseHint = 'Close panel'
      LockHint = 'Lock panel'
      UnlockHint = 'Unlock panel'
      Sections = <>
      SectionLayout.CaptionColor = 16244422
      SectionLayout.CaptionColorTo = 14060643
      SectionLayout.CaptionFontColor = 8661248
      SectionLayout.CaptionRounded = False
      SectionLayout.Corners = scRectangle
      SectionLayout.BackGroundColor = 16248798
      SectionLayout.BackGroundColorTo = 16242365
      SectionLayout.BorderColor = clWhite
      SectionLayout.BorderWidth = 1
      SectionLayout.BackGroundGradientDir = gdVertical
      SectionLayout.Indent = 4
      SectionLayout.Spacing = 4
      SectionLayout.ItemFontColor = 11876608
      SectionLayout.ItemHoverTextColor = 11876608
      SectionLayout.ItemHoverUnderline = True
      SectionLayout.UnderLineCaption = False
      ShowCaptionBorder = False
      ShowClose = False
      ShowLock = False
      Style = esCustom
      Version = '1.5.1.0'
      OrgHeight = 0
      object dbgvariable: TUpWWDbGrid
        Left = 2
        Top = 25
        Width = 1163
        Height = 310
        Selected.Strings = (
          'bianm'#9'10'#9#32534#30721#9#9
          'bianlmc'#9'18'#9#21517#31216#9#9
          'bianlms'#9'32'#9#25551#36848#9#9
          'paixh'#9'18'#9#25490#24207'ID'#9#9
          'shujkjmc'#9'18'#9#25968#25454#25511#20214#21517#31216#9#9
          'laiyzdmc'#9'18'#9#26469#28304#23383#27573#21517#31216#9#9)
        IniAttributes.Delimiter = ';;'
        TitleColor = 16571329
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alClient
        DataSource = dsvariable
        ImeName = #20013#25991' ('#31616#20307') - '#25628#29399#25340#38899#36755#20837#27861
        KeyOptions = []
        RowHeightPercent = 120
        TabOrder = 0
        TitleAlignment = taCenter
        TitleFont.Charset = GB2312_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -14
        TitleFont.Name = #23435#20307
        TitleFont.Style = []
        TitleLines = 1
        TitleButtons = True
        UseTFields = False
      end
    end
  end
  object qry: TUpAdoQuery
    Parameters = <>
    Left = 1056
    Top = 56
  end
  object qrytemp: TUpAdoQuery
    Parameters = <>
    Left = 1088
    Top = 56
  end
  object qrydataset: TUpAdoQuery
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    Parameters = <>
    SQL.Strings = (
      'select * from jc_baobsjj')
    Left = 404
    Top = 217
  end
  object qryvariable: TUpAdoQuery
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from jc_baobbl')
    Left = 404
    Top = 440
  end
  object dsdateset: TUpDataSource
    DataSet = qrydataset
    Left = 436
    Top = 217
  end
  object dsvariable: TUpDataSource
    DataSet = qryvariable
    Left = 440
    Top = 440
  end
  object pmdataset: TPopupMenu
    OnPopup = pmdatasetPopup
    Left = 596
    Top = 289
    object pmnSqlValid: TMenuItem
      Caption = 'Sql'#35821#27861#26816#26597
      OnClick = pmnSqlValidClick
    end
  end
  object frpreport: TfrxReport
    Version = '4.3'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.Author = 'changym'
    ReportOptions.CreateDate = 41037.501118877310000000
    ReportOptions.Description.Strings = (
      'upsoft report design')
    ReportOptions.LastChange = 41525.407687349540000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    StoreInDFM = False
    OnGetValue = frpreportGetValue
    Left = 884
    Top = 161
  end
  object frxdbds_m: TfrxDBDataset
    UserName = 'frxdbds_m'
    CloseDataSource = False
    DataSet = qryreport_m
    Left = 884
    Top = 193
  end
  object frxdbds_d: TfrxDBDataset
    UserName = 'frxdbds_d'
    CloseDataSource = False
    DataSet = qryreport_d
    Left = 916
    Top = 193
  end
  object frxdsgnr1: TfrxDesigner
    DefaultScriptLanguage = 'PascalScript'
    DefaultFont.Charset = DEFAULT_CHARSET
    DefaultFont.Color = clWindowText
    DefaultFont.Height = -13
    DefaultFont.Name = 'Arial'
    DefaultFont.Style = []
    DefaultLeftMargin = 10.000000000000000000
    DefaultRightMargin = 10.000000000000000000
    DefaultTopMargin = 10.000000000000000000
    DefaultBottomMargin = 10.000000000000000000
    DefaultPaperSize = 9
    DefaultOrientation = poPortrait
    Restrictions = []
    RTLLanguage = False
    Left = 916
    Top = 161
  end
  object qryreport_m: TUpAdoQuery
    Parameters = <>
    Left = 884
    Top = 225
  end
  object qryreport_d: TUpAdoQuery
    Parameters = <>
    Left = 916
    Top = 225
  end
  object frxchrtbjct1: TfrxChartObject
    Left = 516
    Top = 268
  end
  object frxcrsbjct1: TfrxCrossObject
    Left = 852
    Top = 164
  end
  object frxdbdtst_3: TfrxDBDataset
    UserName = '3'
    CloseDataSource = False
    OpenDataSource = False
    Left = 951
    Top = 194
  end
  object frxdbdtst_4: TfrxDBDataset
    UserName = '4'
    CloseDataSource = False
    OpenDataSource = False
    Left = 985
    Top = 194
  end
  object frxdbdtst_5: TfrxDBDataset
    UserName = '5'
    CloseDataSource = False
    OpenDataSource = False
    Left = 1020
    Top = 195
  end
  object qryreport_3: TUpAdoQuery
    Parameters = <>
    Left = 951
    Top = 224
  end
  object qryreport_4: TUpAdoQuery
    Parameters = <>
    Left = 985
    Top = 226
  end
  object qryreport_5: TUpAdoQuery
    Parameters = <>
    Left = 1024
    Top = 226
  end
end
