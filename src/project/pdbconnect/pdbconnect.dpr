program pdbconnect;

uses
  Forms,
  SysUtils,
  Windows,
  Dialogs,
  ufrmmain in 'frmmain\ufrmmain.pas' {frmmain},
  ubase in '..\lib\common\ubase.pas',
  udbbase in '..\lib\common\udbbase.pas',
  udbconfigfile in '..\lib\common\udbconfigfile.pas',
  uencode in '..\lib\common\uencode.pas',
  uglobal in '..\lib\common\uglobal.pas',
  umyconfig in '..\lib\common\umyconfig.pas',
  umysystem in '..\lib\common\umysystem.pas',
  utypes in '..\lib\common\utypes.pas',
  ufrmbase in '..\lib\formbase\ufrmbase.pas' {frmbase},
  utblyuang in '..\classes\dbtableclass\utblyuang.pas',
  HzSpell in '..\lib\moudle\huoqupinyin\HzSpell.pas',
  ukeyvaluelist in '..\lib\common\ukeyvaluelist.pas',
  ufrmcomm_showmessage in '..\dllmoudle\common\ufrmcomm_showmessage.pas' {frmcomm_showmessage},
  ucsmsg in '..\classes\csmsg\ucsmsg.pas',
  ufrmcomm_showdialog in '..\dllmoudle\common\ufrmcomm_showdialog.pas' {frmcomm_showdialog};

{$R *.res}

begin
  try
    Application.Initialize;
    Application.CreateForm(Tfrmmain, frmmain);
  Application.CreateForm(Tfrmcomm_showdialog, frmcomm_showdialog);
  except
    on e:exception do
    begin
      MessageDlg(e.Message, mtError, [mbYes], 0);

      application.Terminate;
      exit;
    end;
  end; 

  Application.Run;
end.
