//*******************************************************************
//  pmyhisgroup  pmenzshouf   
//  ufrmdbselectbase.pas     
//                                          
//  创建: changym by 2012-03-04 
//        changup@qq.com                     
//  功能说明:                                            
//      选择类窗体基类, 必须先设置几个必要的参数;
//  修改历史:
//                                            
//  版权所有 (C) 2012 by changym
//                                                       
//*******************************************************************
unit ufrmdbselectbase;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ufrmdbdialogbase, DB, ADODB, Buttons, UpSpeedButton, ExtCtrls,
  UPanel, Grids, Wwdbigrd, Wwdbgrid, frxClass, frxDesgn, frxDBSet,
  UpDataSource, Menus, UpPopupMenu, UpAdoTable, UpAdoQuery, UpWWDbGrid,
  StdCtrls, UpEdit, UpLabel, frxExportXLS, frxExportPDF, ToolPanels,
  UpAdvToolPanel;

type
  Tfrmdbselectbase = class(Tfrmdbdialogbase)
    updvtlpnl1: TUpAdvToolPanel;
    upnltop: TUPanel;
    dbg: TUpWWDbGrid;
    lblsel: TUpLabel;
    edtseljianp: TUpEdit;
    btnselect: TUpSpeedButton;
    procedure dbgDblClick(Sender: TObject);
    procedure dbgKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtseljianpKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private  
    function init() : Boolean;
  public
    strselectsql : string;
    //查询sql语句
    ret_fieldname : string;
    //返回值字段名称
    ret_fieldvalue : string;
    //返回值,客户从这里获取返回值

    //第二个带回的字段 2013-5-2 add by changym
    ret_fieldname_other : string;
    ret_fieldvalue_other : string;
  protected
    function Execute_dll_showmodal_before() : Boolean; override;
    function Execute_dll_showmodal_after() : Boolean; override;
    //用户点击确认返回设置返回信息
  public
    procedure setdbgtitle(index : integer; name : string);
    //设置dbg.title的显示名称
  end;
  
implementation

{$R *.dfm}

{ Tfrmdbselectbase }

function Tfrmdbselectbase.Execute_dll_showmodal_after: Boolean;
begin
  Result := False;

  if ModalResult = mrCancel then Exit;

  if qry.RecordCount = 0 then Exit;

  if Trim(ret_fieldname) = '' then
    ret_fieldname := 'bianm';
  ret_fieldvalue := qry.getString(ret_fieldname);

  //2013-5-2 add by changym
  if Trim(ret_fieldname_other) <> '' then
    ret_fieldvalue_other := qry.getString(ret_fieldname_other);  
  Result := True;
end;

function Tfrmdbselectbase.init: Boolean;
var
  i : Integer;
  bfound : Boolean;
begin
  try
    //打开数据集
    qry.Close;
    qry.SQL.Text := strselectsql;
    qry.Open;

    //2012-10-27:
    //  qry数据集中是否有jianp字段, 显示或者隐藏过滤组件
    bfound := False;
    for i:=0 to qry.fields.Count-1 do
    begin
      if UpperCase(qry.fields[i].FieldName) = 'JIANP' then
      begin
        bfound := True;
        Break;
      end;
    end;

    if not bfound then
    begin
      lblsel.Visible := False;
      edtseljianp.Visible := False;
    end;

    Result := True;
  except
    on e:Exception do
    begin
      self.errmsg := '初始化选择窗体失败:' + e.Message;
      Result := False;
    end;
  end;
end;

procedure Tfrmdbselectbase.setdbgtitle(index: integer; name: string);
begin                     
  dbg.Columns[index].DisplayLabel := name;
  dbg.UseTFields := true;
end;

procedure Tfrmdbselectbase.dbgDblClick(Sender: TObject);
begin
  inherited;

  if qry.Eof then Exit
  else
    btnyesClick(Sender);
end;

procedure Tfrmdbselectbase.dbgKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;

  { 回车模拟yes按钮 }
  if Key = 13 then
    if qry.Eof then Exit
    else
      btnyesClick(Sender);
end;

function Tfrmdbselectbase.Execute_dll_showmodal_before: Boolean;
begin
  Result := False;
  if not init then Exit;

  Result := True;
end;

procedure Tfrmdbselectbase.edtseljianpKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;

  { 过滤
  }
  if edtseljianp.Text='' then
  begin
    { 没有过滤条件了}
    qry.Filtered := False;
    qry.Filter := '';
    qry.Filtered := True;
  end
  else
  begin
    { 按照简拼过滤}
    qry.Filtered := False;
    qry.Filter := 'jianp like ''%' + edtseljianp.Text + '%''';
    qry.Filtered := True;
  end;
end;

end.
