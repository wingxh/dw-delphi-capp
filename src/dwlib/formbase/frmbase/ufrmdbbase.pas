//*******************************************************************
//  ProjectGroup1  pfuzhuang
//  ufrmdbbase.pas
//                                          
//  创建: changym by 2011-11-19
//        changup@qq.com                    
//  新增功能:                                            
//      qry : TUPADOQuery;
//      tbl : TUPADOTable; //如果表通过DbGrid编辑窗体,请用此Tbl关联dbgrid
//      属性DbTblChanged: 标识tbl是否有修改;
//      dbcheck : tdbcheck; //数据库规则检查对象
//      调整窗体caption函数: setcaption2changed() / setcaption2normal();
//  修改历史:
//     2012-7-13:
//       1、废弃了qryreport_var数据集控件的使用, 添加替代品myhisreport_varlist : TKeyValueList;
//          相应修改了变量处理方式;
//  版权所有 (C) 2011 by changym
//                                                       
//*******************************************************************

unit ufrmdbbase;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ufrmbase, DB, ADODB, CnHint, udbcheck, uglobal,
  udlltransfer, utypes, UpAdoQuery, UpAdoTable, UpAdoStoreProc, udmtbl_public,
  umyhisreport, UpDataSource, frxClass, frxDBSet, frxDesgn, ExtCtrls,
  UPanel, Menus, UpPopupMenu, Wwdbgrid, ukeyvaluelist, ComCtrls, upedit,
  frxExportXLS, frxExportCSV, frxExportPDF, UpWWDbGrid;

type
  Tfrmdbbase = class(Tfrmbase)
    qry: TUPADOQuery;
    tbl: TUPADOTable;
    ds: TDataSource;
    qry1: TUPADOQuery;
    qryreport_m: TUPADOQuery;
    dsreport_m: TUpDataSource;
    qryreport_d: TUPADOQuery;
    dsreport_d: TUpDataSource;
    qryreport_vars: TUPADOQuery;
    frxdbds_d: TfrxDBDataset;
    frpusds: TfrxUserDataSet;
    frxdsgnr1: TfrxDesigner;
    frxdbds_m: TfrxDBDataset;
    frpreport: TfrxReport;
    qry2: TUpAdoQuery;
    frxexportpdf: TfrxPDFExport;
    frxexportxls: TfrxXLSExport;
    qrylog: TUpAdoQuery;
    frxdbdtst_3: TfrxDBDataset;
    frxdbdtst_4: TfrxDBDataset;
    frxdbdtst_5: TfrxDBDataset;
    procedure FormCreate(Sender: TObject);
    procedure tblAfterEdit(DataSet: TDataSet);
    procedure tblAfterInsert(DataSet: TDataSet);
    procedure tblAfterDelete(DataSet: TDataSet);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure frpreportGetValue(const VarName: String; var Value: Variant);
    procedure qryreport_mAfterOpen(DataSet: TDataSet);
    procedure qryreport_dAfterOpen(DataSet: TDataSet);
  private       
    FDbTblChanged : Boolean;

  protected
    { 窗体关闭提示语 }
    strwinclose_caption : string;
    strwinclose_text : string;
      
    ffrmchanged : Boolean;
    //窗体是否已经变动; 用于子类中数据维护;
    //通过FrmIsChanged()函数取此属性值
                               
    { 提交提示语 }
    strbtnpost_caption : string;
    strbtnpost_text : string;

    { 取消提示语 }
    strbtncancel_caption : string;
    strbtncancel_text : string;

    {$ifndef mainprog}
    dbcheck : tdbcheck;
    {$endif}
    //数据库检查对象

    dllparams : TDllParam;
    //dll主程序传递的参数集

    dmtbl_public : Tdmtbl_public;

    { 报表相关属性定义
    }
    //报表对象集合; 窗体初始化时通过窗体.name获取
    myhisreportlist : TMyHisReportList;
    //报表变量名-值键值对
    myhisreport_varlist : TKeyValueList;

    // 当前正在打印的报表名称; 默认 = mainreport;
    // 在btnprint按钮按下的时候设置值
    //------------------------------------------------------------------------
    report_currname : string;

    //是否显示打印预览界面; 默认 = true;
    report_bshowpreview : Boolean;
     
  { 打印相关函数
  }
  protected
    {-------------------------------------------------------------------------
     过程名:    PrintBefore
     作者:      changym by 2012.05.06
                changup@qq.com
     参数:
     返回值:    Boolean
     处理过程:
         1、根据属性report_currname获取报表属性;
         2、设置报表控件数据集、变量;
    -------------------------------------------------------------------------}
    function PrintBefore() : Boolean; virtual;

    {-------------------------------------------------------------------------
     过程名:    PrintPost
     作者:      changym by 2012.05.06
                changup@qq.com
     参数:
     返回值:    Boolean
     处理过程:  显示打印预览界面;
    -------------------------------------------------------------------------}
    function PrintPost() : Boolean; virtual;
    {-------------------------------------------------------------------------
     过程名:    PrintPost
     作者:      changym by 2012.05.06
                changup@qq.com
     参数:
     返回值:    Boolean
     处理过程:  打印后的处理; 特殊业务中由子类完成;
    -------------------------------------------------------------------------}
    function PrintAfter() : Boolean; virtual;

    function getDataSetByName(strname : string; var dataset : TUPADOQuery) : Boolean;
    //根据控件名称查找数据库---TUPADOQuery版本
  protected
    procedure setcaption2changed();
    //修改窗体caption到【数据已修改】
    procedure setcaption2normal();
    //修改窗体caption到正常状态

    { 以下的数据操作函数适用于dbgrid方式的数据维护 }
    function oncommitbefore() : Boolean; virtual;
    //提交保存前的处理
    function oncommitafter() : Boolean; virtual;
    //提交保存后的处理
    function commit_change() : Boolean; virtual;
    //提交保存数据变动
    function rollback_change() : Boolean; virtual;
    //取消数据变动
    function onrollbackbefore() : Boolean; virtual;
    //提交取消前的处理
    function onrollbackafter() : Boolean; virtual;
    //提交取消后的处理

  { DLL中窗体,非模态显示相关函数
  }
  protected
    function Execute_dll_show_before() : Boolean; virtual;
    //dll中的窗体在显示前的处理,由子类实现处理;
  public
    procedure Execute_dll_show(dllparam : PDllParam);
    //dll中的窗体,show出来;

  { Dll中窗体,模态显示相关函数
  }
  protected
    function Execute_dll_showmodal_before() : Boolean; virtual;
    //dll中的窗体在显示前的处理,由子类实现处理;
    function Execute_dll_showmodal_after() : Boolean; virtual;
    //dll中的窗体在显示后的处理,由子类实现处理;
  public
    function Execute_dll_showmodal(dllparam : PDllParam) : Boolean;
    //dll中的窗体,ShowModal方式显示;

  public
    function FrmIsChanged() : Boolean;
    //返回窗体ffrmchanged属性值
    procedure CancelAndClose();
    //取消修改立刻关闭窗体; 绕过close中FrmIsChanged的判断
  property
    DbTblChanged : Boolean read FDbTblChanged write FDbTblChanged;
    //dbtbl是否变动属性

  { 写系统日志相关
  }
  public
    function writeLog(caoz: string; yulzfc1:string='';yulzfc2:string='';yulzfc3: string=''): Boolean;
    //写日志

  public
    fselfonlydata : Boolean;
    //是否仅仅自己的数据可见；
    //用于一些跨模块的数据窗口时限制操作员可查询的范围；
  end;

implementation

{$R *.dfm}

{ Tfrmdbbase }

procedure Tfrmdbbase.FormCreate(Sender: TObject);
begin
  inherited;

  fselfonlydata := False;
  //是否默认自己的数据可见
  
  FDbTblChanged := False;
end;

procedure Tfrmdbbase.tblAfterEdit(DataSet: TDataSet);
begin
  inherited;   

  FDbTblChanged := True;
  setcaption2changed();
end;

procedure Tfrmdbbase.tblAfterInsert(DataSet: TDataSet);
begin
  inherited;

  FDbTblChanged := True;
  setcaption2changed();
end;

procedure Tfrmdbbase.tblAfterDelete(DataSet: TDataSet);
begin
  inherited;

  FDbTblChanged := True;
  setcaption2changed();
end;

procedure Tfrmdbbase.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  //销毁公用数据操作对象
  FreeAndNil(dmtbl_public);

  { 已经发生的tbl的修改提示处理;
  }
  if (FDbTblChanged) and tbl.Active then
  begin
    if baseobj.showdialog(strwinclose_caption, strwinclose_text) then
    begin
      try
        //启动事务
        tbl.Connection.BeginTrans;
        //提交前处理
        oncommitbefore();
        //提交数据变动
        commit_change();
        //提交后处理
        oncommitafter();
        //提交事务
        tbl.Connection.CommitTrans;

        //tbl是否变动状态变更
        DbTblChanged := False;
      except
        on e:exception do
        begin
          tbl.Connection.RollbackTrans;
          baseobj.showerror('数据提交发生错误:' + e.Message);
        end;
      end;
    end;
  end;

  {$ifndef mainprog}
  //销毁数据库检查对象
  dbcheck.Free;
  dbcheck := nil;
  {$endif}

  { 销毁报表信息
  }
  myhisreportlist.Free;
  myhisreportlist := nil;
  //报表变量键值对list
  FreeAndNil(myhisreport_varlist);

  inherited;
end;


function Tfrmdbbase.commit_change: Boolean;
begin
  tbl.UpdateBatch();
  Result := True;
end;

function Tfrmdbbase.oncommitafter: Boolean;
begin               
  Result := True;
end;

function Tfrmdbbase.oncommitbefore: Boolean;
begin           
  Result := True;
end;

function Tfrmdbbase.onrollbackafter: Boolean;
begin
  Result := True;
end;

function Tfrmdbbase.onrollbackbefore: Boolean;
begin
  Result := True;
end;

function Tfrmdbbase.rollback_change: Boolean;
begin
  tbl.CancelBatch();
  Result := True;
end;

procedure Tfrmdbbase.setcaption2changed;
begin
  if Pos('数据已修改', self.Caption) <=0 then
    self.Caption := self.Caption + ' 【***数据已修改***】';
end;

procedure Tfrmdbbase.setcaption2normal;
var
  i : integer;
begin
  //从caption中截取掉 【数据已修改】
  i := Pos(' 【***数据已修改***】', self.Caption);
  Self.Caption := Copy(self.Caption, 1, i-1);
end;

procedure Tfrmdbbase.Execute_dll_show(dllparam: PDllParam);
var
  i, m: integer;
begin  
  { 窗体是否被修改 }
  ffrmchanged := False;

  { 创建数据库检查对象
  }
  //dll参数集
  CopyMemory(@dllparams, dllparam, SizeOf(TDllParam));

  { 公用的表操作对象
  }
  dmtbl_public := Tdmtbl_public.Create(@dllparams);

  { 数据库规则检查对象
  }
  {$ifndef mainprog}
  dbcheck := tdbcheck.create(dllparams.padoconn);
  {$endif}

  { 窗体数据集connection属性设置
  }
  for i:=0 to self.ComponentCount-1 do
  begin
    //TADOQUERY
    if UpperCase(Self.Components[i].ClassName) = 'TADOQUERY' then
    begin
      (self.Components[i] as TADOQUERY).Connection := dllparam^.padoconn^;
    end;    
    //TUpAdoQuery
    if UpperCase(Self.Components[i].ClassName) = 'TUPADOQUERY' then
    begin
      (self.Components[i] as TUPADOQUERY).Connection := dllparam^.padoconn^;
    end;
    
    //TADOTABLE
    if UpperCase(Self.Components[i].ClassName) = 'TADOTABLE' then
    begin
      (self.Components[i] as TADOTABLE).Connection := dllparam^.padoconn^;
    end;
    //TUPADOTABLE
    if UpperCase(Self.Components[i].ClassName) = 'TUPADOTABLE' then
    begin
      (self.Components[i] as TUPADOTABLE).Connection := dllparam^.padoconn^;
    end;

    //TADOSTOREDPROC
    if UpperCase(self.Components[i].ClassName) = 'TADOSTOREDPROC' then
    begin
      (Self.Components[i] as TADOStoredProc).Connection := dllparam^.padoconn^;
    end;    
    //TUPADOSTOREPROC
    if UpperCase(self.Components[i].ClassName) = 'TUPADOSTOREPROC' then
    begin
      (Self.Components[i] as TUpAdoStoreProc).Connection := dllparam^.padoconn^;
    end;

    //TUpWWDbGrid
    if UpperCase(self.Components[i].ClassName) = 'TUPWWDBGRID' then
    begin
      (Self.Components[i] as TUpWWDbGrid).PaintOptions.AlternatingRowColor :=
        StringToColor(dllparams.mysystem^.global_sysparams.valueitem('dbg_AlternatingRowColor'));
    end;

    //Tframebase
    if (self.Components[i].InheritsFrom(TFrame)) then
    begin
      for m:=0 to (self.Components[i] as TFrame).ComponentCount-1 do
      begin
        if UpperCase((self.Components[i] as TFrame).Components[m].ClassName)='TUPWWDBGRID' then
        begin
          ((self.Components[i] as TFrame).Components[m] as TUpWWDbGrid).PaintOptions.AlternatingRowColor :=
            StringToColor(dllparams.mysystem^.global_sysparams.valueitem('dbg_AlternatingRowColor'));
        end;
      end;
    end;
  end;

  { 报表集合操作
  }
  //创建报表集合
  myhisreportlist := TMyHisReportList.Create(dllparams.padoconn);
  //获取窗体报表信息
  if not myhisreportlist.getReportInfoByFormName(self.ClassName) then
  begin
    baseobj.showerror('获取窗体报表信息失败:' + myhisreportlist.errmsg);
  end;

  //报表变量键值对list创建
  myhisreport_varlist := TKeyValueList.Create;
  
  //默认报表名称
  report_currname := 'mainreport';
  
  //是否显示打印预览界面; 默认 = true;
  //report_bshowpreview := True;
  //改由参数控制是否显示打印预览窗体
  report_bshowpreview :=
     SameText(dllparams.mysystem^.local_sysparams.valueitem('report_bshowpreview'), '1');

  //窗体提示语默认值
  //2012-11-3：获取窗体关闭提示消息；
  qry1.Close;
  qry1.SQL.Text := 'select * from xt_chuangtxxc where chuangt=''' +
      Self.ClassName + '''';
  qry1.Open;
  if qry1.RecordCount = 0 then
  begin
    //窗体关闭提示语
    strwinclose_caption := '系统提示';
    strwinclose_text := '数据已修改,保存本次修改吗？';
                        
    //设置提交按钮默认提示语
    strbtnpost_caption := '业务提交提示';
    strbtnpost_text := '确认提交保存吗?';

    //设置取消按钮默认提示语
    strbtncancel_caption := '业务取消提示';
    strbtncancel_text := '确认放弃当前修改吗？';
  end
  else
  begin
    //窗体关闭提示语
    strwinclose_caption := qry1.fieldbyname('winclose_caption').AsString;
    strwinclose_text := qry1.fieldbyname('winclose_text').AsString;

    //设置提交按钮默认提示语
    strbtnpost_caption := qry1.fieldbyname('btnpost_caption').AsString;
    strbtnpost_text := qry1.fieldbyname('btnpost_text').AsString;

    //设置取消按钮默认提示语
    strbtncancel_caption := qry1.fieldbyname('btncancel_caption').AsString;
    strbtncancel_text := qry1.fieldbyname('btncancel_text').AsString;
  end;
  qry1.close;


  //显示前的处理
  if not Execute_dll_show_before() then Exit;

  Show();
end;

function Tfrmdbbase.Execute_dll_show_before: Boolean;
begin
  Result := True;
end;

function Tfrmdbbase.Execute_dll_showmodal(dllparam: PDllParam): Boolean;
var
  i: integer;
begin        
  { 窗体是否被修改 }
  ffrmchanged := False;
    
  { 创建数据库检查对象
  }
  //dll参数集
  CopyMemory(@dllparams, dllparam, SizeOf(TDllParam));

  //公用的表操作对象
  dmtbl_public := Tdmtbl_public.Create(@dllparams);

  {$ifndef mainprog}
  //数据库规则检查对象
  dbcheck := tdbcheck.create(dllparams.padoconn);
  {$endif}
  
  { 窗体数据集connection属性设置
  }
  for i:=0 to self.ComponentCount-1 do
  begin
    //TUPADOQuery
    if UpperCase(Self.Components[i].ClassName) = 'TUPADOQuery' then
    begin
      (self.Components[i] as TUPADOQuery).Connection := dllparam^.padoconn^;
    end;    
    //TUpAdoQuery
    if UpperCase(Self.Components[i].ClassName) = 'TUPADOQUERY' then
    begin
      (self.Components[i] as TUPADOQUERY).Connection := dllparam^.padoconn^;
    end;

    //TUPADOTable
    if UpperCase(Self.Components[i].ClassName) = 'TUPADOTable' then
    begin
      (self.Components[i] as TUPADOTable).Connection := dllparam^.padoconn^;
    end;
    //TUPADOTABLE
    if UpperCase(Self.Components[i].ClassName) = 'TUPADOTABLE' then
    begin
      (self.Components[i] as TUPADOTABLE).Connection := dllparam^.padoconn^;
    end;

    //TADOSTOREDPROC
    if UpperCase(self.Components[i].ClassName) = 'TADOSTOREDPROC' then
    begin
      (Self.Components[i] as TADOStoredProc).Connection := dllparam^.padoconn^;
    end;    
    //TUPADOSTOREPROC
    if UpperCase(self.Components[i].ClassName) = 'TUPADOSTOREPROC' then
    begin
      (Self.Components[i] as TUpAdoStoreProc).Connection := dllparam^.padoconn^;
    end;

    //debug：2012-6-30, 借道设置一下dbgrid的字体
    {
    if UpperCase(self.Components[i].ClassName) = 'TUpWWDbGrid' then
    begin
      (Self.Components[i] as TUpWWDbGrid).Font.Size := 12;
      (Self.Components[i] as TUpWWDbGrid).TitleFont.Size := 12;
    end;
    }
  end;

  { 报表集合操作
  }
  //创建报表集合
  myhisreportlist := TMyHisReportList.Create(dllparams.padoconn);
  //获取窗体报表信息
  if not myhisreportlist.getReportInfoByFormName(self.ClassName) then
  begin
    baseobj.showerror('获取窗体报表信息失败:' + myhisreportlist.errmsg);
  end;
  
  //报表变量键值对list创建
  myhisreport_varlist := TKeyValueList.Create;

  //默认报表名称
  report_currname := 'mainreport';
  //是否显示打印预览界面; 默认 = true;
  //report_bshowpreview := True;
  //改由参数控制是否显示打印预览窗体
  if dllparams.mysystem <> nil then //有些简捷的调用会传nil值过来;比如服务器的在线用户窗体;
  begin
    report_bshowpreview :=
       SameText(dllparams.mysystem^.local_sysparams.valueitem('report_bshowpreview'), '1');
  end
  else
  begin
    report_bshowpreview := True;
  end;

  //窗体提示语默认值
  //2012-11-3：获取窗体关闭提示消息；
  qry1.Close;
  qry1.SQL.Text := 'select * from xt_chuangtxxc where chuangt=''' +
      Self.ClassName + '''';
  qry1.Open;
  if qry1.RecordCount = 0 then
  begin
    //窗体关闭提示语
    strwinclose_caption := '系统提示';
    strwinclose_text := '数据已修改,保存本次修改吗？';
                        
    //设置提交按钮默认提示语
    strbtnpost_caption := '业务提交提示';
    strbtnpost_text := '确认提交保存吗?';

    //设置取消按钮默认提示语
    strbtncancel_caption := '业务取消提示';
    strbtncancel_text := '确认放弃当前修改吗？';
  end
  else
  begin
    //窗体关闭提示语
    strwinclose_caption := qry1.fieldbyname('winclose_caption').AsString;
    strwinclose_text := qry1.fieldbyname('winclose_text').AsString;

    //设置提交按钮默认提示语
    strbtnpost_caption := qry1.fieldbyname('btnpost_caption').AsString;
    strbtnpost_text := qry1.fieldbyname('btnpost_text').AsString;

    //设置取消按钮默认提示语
    strbtncancel_caption := qry1.fieldbyname('btncancel_caption').AsString;
    strbtncancel_text := qry1.fieldbyname('btncancel_text').AsString;
  end;
  qry1.close;
  
  //显示前的处理
  if Execute_dll_showmodal_before() then
  begin
    ShowModal();
    Result := Execute_dll_showmodal_after();
  end
  else
  begin
    Result := False;
  end;
end;

function Tfrmdbbase.Execute_dll_showmodal_before: Boolean;
begin
  Result := True;
end;

function Tfrmdbbase.Execute_dll_showmodal_after: Boolean;
begin
  Result := False;
end;

function Tfrmdbbase.PrintAfter: Boolean;
begin  
  { 2012-1-27 打印前数据集dataset.disablecontrols;
    打印后数据集.enablecontrols;
  }
  if frxdbds_m.DataSet <> nil then
    frxdbds_m.DataSet.EnableControls;
  if frxdbds_d.DataSet <> nil then
    frxdbds_d.DataSet.EnableControls;

  Result := True;
end;

function Tfrmdbbase.PrintBefore: Boolean;
var
  report_filename : string;
  i : Integer;
  report : TMyHisReport;
  qry_temp : TUPADOQuery;
  pfrxdbds: PfrxDBDataset;
begin
  Result := False;

  { 报表打印前预处理
  }
  //查找当前报表是否定义;
  if not myhisreportlist.getReportByName(report_currname, report) then
  begin
    baseobj.showerror('报表' + report_currname + '未定义！');
    Exit;
  end;   
        
  //合成报表文件路径
  report_filename := ExtractFilePath(Application.ExeName) +
      '\reports\' + self.ClassName + '_' + report_currname + '.fr3';
  if not FileExists(report_filename) then
  begin
    baseobj.showerror('报表文件' + report_filename + '未找到！');
    Exit;
  end;

  //加载报表
  frpreport.LoadFromFile(report_filename);

  //加载报表变量
  frpreport.Variables.Clear;
  for i:=0 to report.variablelist.Count-1 do
  begin
    frpreport.Variables.Add.Name := ' 参数';
    frpreport.Variables.AddVariable('参数',
        TMyHisReportVariable(report.variablelist.Items[i]).bianlmc,
        TMyHisReportVariable(report.variablelist.Items[i]).bianlms);
  end;

  //清空报表数据集集合
  frpreport.DataSets.Clear;
  for i:=0 to report.datasetlist.Count-1 do
  begin
    qry_temp := nil;
    //查找用户指定的数据集控件
    if not getDataSetByName(TMyHisReportDataset(report.datasetlist.Items[i]).shujkjmc,
        qry_temp) then
    begin
      baseobj.showerror('指定的数据控件' +
          TMyHisReportDataset(report.datasetlist.Items[i]).shujkjmc +
          '未找到！');
      Exit;
    end;

    //数据集是否已经打开
    if not qry_temp.Active then
    begin
      baseobj.showerror('【' + TMyHisReportDataset(report.datasetlist.Items[i]).shujjms +
          '】尚未打开,请先查询数据!');
      Exit;
    end;
    pfrxdbds := nil;
    case i of
      0:
      begin  //第一个数据集
        pfrxdbds := @frxdbds_m;
      end;
      1:
      begin  //第二个数据集
        pfrxdbds := @frxdbds_d;
      end;
      2:
      begin
        pfrxdbds:= @frxdbdtst_3;
      end;
      3:
      begin
        pfrxdbds:= @frxdbdtst_4;
      end;
      4:
      begin
        pfrxdbds:= @frxdbdtst_5;
      end;
      else
      begin
        baseobj.showwarning('报表定义的数据集个数超过了最大个数,打印结果可能不正确！');
      end;
    end;
    if pfrxdbds <> nil then
    begin
      pfrxdbds^.DataSet := qry_temp;
      pfrxdbds^.UserName := TMyHisReportDataset(report.datasetlist.Items[i]).shujjyhmc;
      pfrxdbds^.Description := TMyHisReportDataset(report.datasetlist.Items[i]).shujjms;
      //打印记录开始点设置
      case TMyHisReportDataset(report.datasetlist.Items[i]).dayjlksd of
        1: //开始
        begin
          pfrxdbds^.RangeBegin := rbFirst;
        end;
        2: //当前
        begin
          pfrxdbds^.RangeBegin := rbCurrent;
        end;
        else
        begin
          baseobj.showwarning('数据集打印记录开始点' +
              IntToStr(TMyHisReportDataset(report.datasetlist.Items[i]).dayjlksd) +
              '设置错误,打印可能不正确!');
        end;
      end;         
      //打印记录结束点设置
      case TMyHisReportDataset(report.datasetlist.Items[i]).dayjljsd of
        2: //当前
        begin
          pfrxdbds^.RangeEnd := reCurrent;
        end;
        3:
        begin
          pfrxdbds^.RangeEnd := reLast;
        end;
        4:
        begin
          pfrxdbds^.RangeEnd := reCount;
        end;
        else
        begin
          baseobj.showwarning('数据集打印记录开始点' +
              IntToStr(TMyHisReportDataset(report.datasetlist.Items[i]).dayjlksd) +
              '设置错误,打印可能不正确!');
        end;
      end;
      //打印记录条数
      pfrxdbds^.RangeEndCount := TMyHisReportDataset(report.datasetlist.Items[i]).dayjlts;

      frpreport.DataSets.Add(pfrxdbds^);
    end;
  end;

  //报表变量键值对list初始化; 这样的话子类在第一行就要调用父类此函数
  myhisreport_varlist.clearitem;
  //2013-1-21：添加公用的报表变量
  //系统用户名称
  myhisreport_varlist.additem('用户名称', dllparams.mysystem^.cliendname);
  //操作员
  myhisreport_varlist.additem('操作员', dllparams.mysystem^.loginyuang.xingm);
  
  { 打开变量参数数据集;
    定制的qryreport_vars.sql.text在子类的printbefore第一行首选执行;
    2012-7-13：废弃的代码, 用上面个的myhisreport_varlist代替此存储方式
  qryreport_vars.Close;
  if Trim(qryreport_vars.SQL.Text) = '' then
    qryreport_vars.SQL.Text := 'select ''' + dllparams.mysystem.loginyuang.xingm + '''' +
        ' 制单人';
  qryreport_vars.Open;                                                
  }

  { 2012-1-27 打印前数据集dataset.disabled;
  }
  if frxdbds_m.DataSet <> nil then
    frxdbds_m.DataSet.DisableControls;
  if frxdbds_d.DataSet <> nil then
    frxdbds_d.DataSet.DisableControls;

  Result := True;
end;

function Tfrmdbbase.PrintPost: Boolean;
begin
  //frpreport.DesignReport();
  //Exit;
  
  //调用报表打印预览界面
  if report_bshowpreview then
  begin
    //显示打印预览窗体; 再打印;
    frpreport.PrepareReport;
    frpreport.ShowPreparedReport;
  end
  else
  begin
    //直接打印
    frpreport.PrintOptions.ShowDialog := False;
    frpreport.PrepareReport;
    frpreport.Print;
  end;

  Result := True;
end;

function Tfrmdbbase.getDataSetByName(strname: string;
  var dataset: TUPADOQuery): Boolean;
var
  i, m : Integer;
  bfound : Boolean;
begin
  bfound := False;

  for i:=0 to self.ComponentCount-1 do
  begin
    if (Components[i] is TUPADOQuery) then
    begin
      //是TUPADOQuery
      if UpperCase((Components[i] as TUPADOQuery).Name) =
          UpperCase(strname) then
      begin
        //找到
        bfound := True;
        dataset := TUPADOQuery(Components[i]);
        Break;
      end;
    end;

    //TFrame
    if Components[i] is TFrame then
    begin
      for m:=0 to (Components[i] as TFrame).ComponentCount-1 do
      begin
        if ((Components[i] as TFrame).Components[m] is TUPADOQuery) then
        begin
          //是TUPADOQuery
          if UpperCase((((Components[i] as TFrame).Components[m]) as TUPADOQuery).Name) =
              UpperCase(strname) then
          begin
            //找到
            bfound := True;
            dataset := TUPADOQuery((Components[i] as TFrame).Components[m]);
            Break;
          end;
        end;
      end;  
    end;
  end;

  Result := (bfound = True);
end;

{ 2012-7-13：
      修改报表框架自定义变量存储方式后废弃的代码段, 暂时保留, 新代码
  测试通过即删除
procedure Tfrmdbbase.frpreportGetValue(const VarName: String;
  var Value: Variant);
var
  i : integer;
  bfound : Boolean;
  report : TMyHisReport;
  variable : TMyHisReportVariable;
  qry_temp : TUPADOQuery;
begin
  // 报表变量值处理

  //查找当前报表是否定义;
  if not myhisreportlist.getReportByName(report_currname, report) then
  begin
    baseobj.showerror('报表' + report_currname + '未定义！');
    Exit;
  end;

  if not report.getVariable(VarName, variable) then
  begin
    Value := '   ';
  end
  else
  begin
    if not getDataSetByName(variable.shujkjmc, qry_temp) then
    begin
      Value := '   ';
    end
    else
    begin
      bfound := False;
      for i:=0 to qry_temp.FieldCount-1 do
      begin
        if UpperCase(qry_temp.Fields[i].FieldName) =
            UpperCase(variable.laiyzdmc) then
        begin
          bfound := true;
          Value := qry_temp.fieldbyname(variable.laiyzdmc).AsString;
          Break;
        end;
      end;

      if not bfound then Value := '   ';
    end;
  end;
end;
}

procedure Tfrmdbbase.frpreportGetValue(const VarName: String;
  var Value: Variant);
var
  report : TMyHisReport;
  variable : TMyHisReportVariable;
begin
  // 报表变量值处理

  //查找当前报表是否定义;
  if not myhisreportlist.getReportByName(report_currname, report) then
  begin
    baseobj.showerror('报表' + report_currname + '未定义！');
    Exit;
  end;

  if not report.getVariable(VarName, variable) then
  begin
    { 2013-2-26: bug修复 by changym
      //Value := '  '; 这句赋为空导致报表所有的计算对象全部为空了; 目前的现象是
      //  表达式失效; 系统自带的count函数失效, 修复为//Value := value后正常;
    }
    Value := Value;
  end
  else
  begin
    Value := myhisreport_varlist.valueitem(variable.bianlmc);
  end;
end;

function Tfrmdbbase.FrmIsChanged: Boolean;
begin
  Result := ffrmchanged;
end;

procedure Tfrmdbbase.CancelAndClose;
begin
  ffrmchanged := False;
  Close;
end;

procedure Tfrmdbbase.qryreport_mAfterOpen(DataSet: TDataSet);
var
  i : Integer;
begin
  inherited;

  //设置float字段的格式
  for i:=0 to DataSet.Fields.Count-1 do
  begin
    if DataSet.Fields[i] is TBCDField then
    begin
      (DataSet.Fields[i] as TNumericField).DisplayFormat := '0.0000';
    end;
  end;
end;

procedure Tfrmdbbase.qryreport_dAfterOpen(DataSet: TDataSet);
var
  i : Integer;
begin
  inherited;

  //设置float字段的格式
  for i:=0 to DataSet.Fields.Count-1 do
  begin
    if DataSet.Fields[i] is TBCDField then
    begin
      (DataSet.Fields[i] as TNumericField).DisplayFormat := '0.0000';
    end;
  end;
end;

function Tfrmdbbase.writeLog(caoz, yulzfc1, yulzfc2, yulzfc3: string): Boolean;
begin
  try
    qrylog.ExecuteSql(Format('insert into sys_riz_caoz(chuangklm,caozybm' +
        ',caozyxm,caozsj,caoz,yulzfc1,yulzfc2,yulzfc3)' +
        ' values(''%s'',%d,''%s'',getdate(),''%s'',''%s'',''%s'',''%s'')',
        [LowerCase(self.ClassName),dllparams.mysystem^.loginyuang.bianm,
         dllparams.mysystem^.loginyuang.xingm, caoz, yulzfc1, yulzfc2, yulzfc3]));
    Result:= True;
  except
    on E:Exception do
    begin
      Result:= False;
      errmsg:= e.Message;
    end;
  end;
end;

end.
