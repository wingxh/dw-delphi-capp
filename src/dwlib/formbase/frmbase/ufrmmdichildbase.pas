//*******************************************************************
//  ProjectGroup1  pfuzhuang   
//  ufrmmdichildbase.pas
//  MDI child 基类窗体     
//
//  功能：
//    FormStyle := fsMDIChild;
//    Visable := True; 配合上面的属性变化;
//    FormCreate事件: 将left\top修改为0;
//
//  创建: changym by 2011-11-21
//        changup@qq.com；
//
//  版权所有 (C) 2011 by changym
//                                                       
//*******************************************************************

unit ufrmmdichildbase;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ufrmbase, ufrmdbbase, DB, ADODB, CnHint, UpDataSource, frxClass,
  frxDBSet, frxDesgn, ExtCtrls, UPanel, Menus, UpPopupMenu, UpAdoTable,
  UpAdoQuery, utypes, ComCtrls, frxExportXLS, frxExportCSV, frxExportPDF;

type
  Tfrmmdichildbase = class(Tfrmdbbase)
    mnchuangtzuijdax: TMenuItem;
    N1: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
  private

  protected
    function Execute_dll_show_before() : Boolean; override;
  
  end;

implementation

{$R *.dfm}

procedure Tfrmmdichildbase.FormCreate(Sender: TObject);
begin
  inherited;

  { 调整窗体位置到0,0坐标
  }
  Left := 0;
  Top := 0;
end;

procedure Tfrmmdichildbase.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;

  //给主窗体发送关闭消息
  SendMessage(dllparams.papp^.MainForm.Handle, UPM_MDICHILD_ONCLOSE, 0, 0);
end;

function Tfrmmdichildbase.Execute_dll_show_before: Boolean;
var
  w, h : Integer;
begin
  Result := inherited Execute_dll_show_before;

  w := dllparams.papp^.MainForm.ClientWidth-5;
  h := dllparams.papp^.MainForm.ClientHeight-50-30-8;
  self.Width := w;
  self.Height := h;
   
  WindowState := wsMaximized;
end;

procedure Tfrmmdichildbase.FormActivate(Sender: TObject);
begin
  inherited;

  WindowState := wsMaximized;
end;

end.
