unit ufrmflash;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ufrmbase, ExtCtrls, StdCtrls, CnHint, Menus, UpPopupMenu, jpeg;

type
  Tfrmflash = class(Tfrmbase)
    pclient: TPanel;
    pbottom: TPanel;
    lblmsg: TLabel;
    imgflash: TImage;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    procedure loadimg(imgfilename : string);
    //加载flash图片
    procedure setmsg(strmsg : string);
    //设置提示信息
    
  end;

var
  frmflash: Tfrmflash;

implementation

{$R *.dfm}

{ Tfrmflash }

procedure Tfrmflash.loadimg(imgfilename: string);
begin
  imgflash.Picture.LoadFromFile(imgfilename);
end;

procedure Tfrmflash.setmsg(strmsg: string);
begin
  lblmsg.Caption := strmsg;
end;

procedure Tfrmflash.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;

  frmflash := nil;
end;

end.
