unit ufrmcomm_showdialog;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Buttons, UpSpeedButton, ExtCtrls, UPanel, UpImage, StdCtrls,
  UpLabel, UpCheckBox;

type
  Tfrmcomm_showdialog = class(TForm)
    bvl1: TBevel;
    lbltext: TUpLabel;
    img1: TUpImage;
    pfrmtop: TUPanel;
    btnok: TUpSpeedButton;
    btncancel: TUpSpeedButton;
    chkOK: TUpCheckBox;
    procedure btncancelClick(Sender: TObject);
    procedure btnokClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure chkOKKeyPress(Sender: TObject; var Key: Char);
  private
  public
    procedure setmessage(strcaption, strtext : string; btncount : integer);
  end;

var
  frmcomm_showdialog: Tfrmcomm_showdialog;

implementation

{$R *.dfm}

procedure Tfrmcomm_showdialog.btncancelClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

procedure Tfrmcomm_showdialog.btnokClick(Sender: TObject);
begin
  if not chkOK.Checked then
  begin
    ShowMessage('请勾选确认操作框确认当前操作！');
    chkOK.SetFocus;
    exit;
  end;
  
  ModalResult := mrOk;
end;

procedure Tfrmcomm_showdialog.FormKeyPress(Sender: TObject; var Key: Char);
begin
  //回车键--->OK按钮
  if Key = #13 then
    btnokClick(Sender);

  //ESC--->Cancel按钮
  if Key = #27 then
    btncancelClick(Sender);
end;

procedure Tfrmcomm_showdialog.setmessage(strcaption, strtext: string;
  btncount: integer);
begin
  self.caption := strcaption;
  lbltext.Caption := strtext;
  chkOK.Checked := False;
end;

procedure Tfrmcomm_showdialog.chkOKKeyPress(Sender: TObject;
  var Key: Char);
begin
  FormKeyPress(chkOK, key);
end;

end.
